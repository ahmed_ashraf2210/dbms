package eg.edu.alexu.csd.oop.draw;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.Ellipse2D;
import java.util.HashMap;
import java.util.Map;

public class Circle extends Ellipse{

	
	
	public void setProperties(Map<String, Double> properties) {
		// TODO Auto-generated method stub
		
		Double x1= properties.get("xAxis");
		this.properties.put("xAxis", x1);
		Double y1= properties.get("yAxis");
		this.properties.put("yAxis", y1);
	}
	
	public void draw(Graphics canvas) {
			// TODO Auto-generated method stub
	        	if(!properties.isEmpty()) {
	           int x = (int) Math.round(properties.get("xAxis")); // 3
	           int y = (int) Math.round(properties.get("yAxis"));
	           
	           Point p = new Point();
	           p = getPosition();
	           canvas.setColor(this.color);
	           if(x<p.x&&y<p.y){
	        	 
	        	   if(this.fill)
	               canvas.fillOval(x , y , p.x-x, p.x-x);
	        	   else 
	                   canvas.drawOval(x , y , p.x-x, p.x-x);

	           }
	           else if(x<p.x&&y>p.y){
	        	   if(this.fill)
	           canvas.fillOval(x , p.y , p.x-x, p.x-x);
	        	   else    
	                   canvas.drawOval(x , p.y , p.x-x, p.x-x);

	           }
	           else if(x>p.x&&y<p.y){
	        	   if(this.fill)
	               canvas.fillOval(p.x , y , x-p.x, x-p.x);
	        	   else    
	                   canvas.drawOval(p.x , y , x-p.x, x-p.x);

	           }
	           else if(x>p.x&&y>p.y){
	        	   if(this.fill)
	               canvas.fillOval(p.x , p.y , x-p.x, x-p.x);
	        	   else
	                   canvas.drawOval(p.x , p.y , x-p.x, x-p.x);

	           }
	  }
	        }

	}
