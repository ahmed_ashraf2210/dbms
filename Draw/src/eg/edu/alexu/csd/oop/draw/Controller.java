package eg.edu.alexu.csd.oop.draw;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;

import java.awt.BorderLayout;

import javax.swing.JButton;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;

import javax.swing.JTextField;
import javax.swing.JLabel;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.border.BevelBorder;

import eg.edu.alexu.csd.oop.test.DummyShape;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.management.RuntimeErrorException;
import javax.swing.ImageIcon;

import java.awt.event.MouseMotionAdapter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

import javax.swing.JPanel;

public class Controller implements DrawingEngine {
	public int x = 0;
	Double firstx;
	Double firsty;
	Double secondx;
	Double secondy;
	Double thirdx;
	Double thirdy;
	boolean f = false;
	Map<String, Double> p = new HashMap<>();

	public List<Shape> shapes = new ArrayList<>();
	public List<Shape> redoShapes = new ArrayList<>();
	public List<Shape> removedShapes = new ArrayList<>();
	public List<Shape> updatedShapes = new ArrayList<>();
	
	int breakPoint = -1;
	boolean flag = false;

	@Override
	public void refresh(Graphics canvas) {
		if (!(shapes.size() == 0) &&!shapes.get(0).getProperties().isEmpty()) {
			canvas.clearRect(0, 0, 2000, 2000);
		}

		if(shapes.size() == 0) {
			canvas.clearRect(0, 0, 2000, 2000);
		}
		for (int i = 0; i < shapes.size(); i++) {

			Shape s = (Shape) shapes.get(i);
			s.draw(canvas);

		}

	}

	@Override
	public void addShape(Shape shape) {
		if (x != 0 && ((shape.getProperties().isEmpty() || ((AbstractShape) shape).type == 0))) {
			return;
		}
		p = shape.getProperties();

		// line
		if (p != null && p.containsKey("startPointx") && p.containsKey("startPointy")
				&& !p.containsKey("secondPointy")) {
			firstx = p.get("startPointx");
			firsty = p.get("startPointy");

			secondx = p.get("endPointx");
			secondy = p.get("endPointy");

			double slope = (secondy - firsty) / (secondx - firstx);

			p.put("slope", slope);
			shape.setProperties(p);
		}

		else if (p != null && p.containsKey("endx")) {
			firstx = shape.getPosition().getX();
			firsty = shape.getPosition().getY();

			secondx = p.get("endx");
			secondy = p.get("endy");
			double slope = 0;

			if (!secondx.equals(firstx)) {
				slope = (secondy - firsty) / (secondx - firstx);
			}

			p.put("slope", slope);
			shape.setProperties(p);
		} else if (p != null && p.containsKey("xAxis")) {

			firstx = shape.getPosition().getX();
			firsty = shape.getPosition().getY();

			secondx = p.get("xAxis");
			secondy = p.get("yAxis");
			double slope = 0;

			if (!secondx.equals(firstx)) {
				slope = (secondy - firsty) / (secondx - firstx);
			}

			p.put("slope", slope);
			shape.setProperties(p);

		} else if (p != null && p.containsKey("secondPointx")) {

			firstx = p.get("startPointx");
			firsty = p.get("startPointy");

			secondx = p.get("endPointx");
			secondy = p.get("endPointy");

			thirdx = p.get("secondPointx");
			thirdy = p.get("secondPointy");

			double slope = (secondy - firsty) / (secondx - firstx);
			double slope2 = (thirdy - firsty) / (thirdx - firstx);

			p.put("slope1", slope);
			p.put("slope2", slope2);

			shape.setProperties(p);

		}

		shapes.add(shape);
		if (flag) {
			breakPoint = shapes.indexOf(shape);
			flag = false;
		}
		redoShapes.clear();
		removedShapes.clear();
		updatedShapes.clear();

	}

	@Override
	public void removeShape(Shape shape) {
		shapes.remove(shape);
		removedShapes.add(shape);

	}

	@Override
	public void updateShape(Shape oldShape, Shape newShape) {
		int i = 0;
		for (i = 0; i < shapes.size(); i++) {

			if (shapes.get(i).equals(oldShape))
				break;

		}
		int h;
		shapes.set(i, newShape);
		if(updatedShapes.isEmpty()) {
			updatedShapes.add(oldShape);
			updatedShapes.add(newShape);
		}
		else {
			updatedShapes.remove(oldShape);
			updatedShapes.remove(newShape);
		}
		
		
	}

	@Override
	public Shape[] getShapes() {
		Shape[] s = new Shape[shapes.size()];

		for (int i = 0; i < shapes.size(); i++) {
			Shape s1 = (Shape) shapes.get(i);
			s[i] = s1;
		}
		return s;

	}

	@Override
	public List<Class<? extends Shape>> getSupportedShapes() {

		List<Class<? extends Shape>> supportedShapes = new ArrayList<>();
		try {
			JarFile jarFile = new JarFile("RoundRectangle.jar");
			Enumeration<JarEntry> e = jarFile.entries();

			URL[] urls = { new URL("jar:file:" + "RoundRectangle.jar" + "!/") };
			URLClassLoader cl = URLClassLoader.newInstance(urls);

			while (e.hasMoreElements()) {
				JarEntry je = e.nextElement();
				if (je.isDirectory() || !je.getName().endsWith(".class")) {
					continue;
				}
				String className = je.getName().substring(0, je.getName().length() - 6);
				className = className.replace('/', '.');
				Class<? extends Shape> c = (Class<? extends Shape>) cl.loadClass(className);

				Object a = c.newInstance();
				if (a instanceof Shape) {
					supportedShapes.add(c);
				} else {
					throw new RuntimeErrorException(null, "Must be of Type Shape");
				}

			}
		} catch (Exception e) {
			// TODO: handle exception
		}

		return supportedShapes;

	}

	@Override
	public void undo() {
		if (!shapes.isEmpty() && redoShapes.size() != 20 && breakPoint != shapes.size()) {
			flag = true;
			boolean f = false;
			
			
			for(int i = 0; i < removedShapes.size(); i++) {
				shapes.add(removedShapes.remove(i));
				f = true;
			}
			if(!f) {
				if(updatedShapes.isEmpty()) {
					redoShapes.add(shapes.remove(shapes.size() - 1));
				}
				else {
					Shape s = new DummyShape();
					Shape s2 = new DummyShape();
					s = updatedShapes.get(updatedShapes.size()-1);
					s2 = updatedShapes.get(updatedShapes.size()-2);
					
					updateShape(s, s2);
				}
			}
			
		}

	}

	@Override
	public void redo() {
		if (!redoShapes.isEmpty()) {
			shapes.add(redoShapes.remove(redoShapes.size() - 1));

		}

	}

	@Override
	public void save(String path) {
		for (int i = 0; i < getShapes().length; i++) {

		}
		if (path.charAt(path.length() - 1) == 'n' || path.charAt(path.length() - 1) == 'N') {
			json b = new json();
			b.write(getShapes(), path);
		}
		if (path.charAt(path.length() - 1) == 'l' || path.charAt(path.length() - 1) == 'L') {
			xml b = new xml();
			b.write(getShapes(), path);
		}
	}

	@Override
	public void load(String path) {

		if (path.charAt(path.length() - 1) == 'n' || path.charAt(path.length() - 1) == 'N') {
			json b = new json();
			try {
				shapes = b.read(path);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (path.charAt(path.length() - 1) == 'l' || path.charAt(path.length() - 1) == 'L') {
			xml b = new xml();
			try {
				shapes = b.read(path);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		redoShapes.clear();
		breakPoint = shapes.size();

	}

	public Shape select(Point screen) {

		for (int i = 0; i < shapes.size(); i++) {

			AbstractShape s = (AbstractShape) shapes.get(i);
			if (s.isSelected(screen)) {
				System.out.println("shape is selected");
				return s;

			}
		}
		return null;

	}

}
