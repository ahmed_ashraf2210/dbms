package eg.edu.alexu.csd.oop.draw;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JColorChooser;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.MouseInfo;
import java.awt.Point;

import javax.swing.JTextField;
import javax.swing.JLabel;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.JFileChooser;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.ImageIcon;
import javax.swing.Timer;

import java.awt.event.KeyListener;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseWheelListener;
import java.awt.event.MouseWheelEvent;

import javax.swing.JSlider;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;
import java.io.File;

import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import javax.swing.filechooser.FileSystemView;

import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

import javax.swing.JRadioButton;
import javax.swing.JList;

public class DrawSample1 {
	//main
	boolean select = false;
	Point selectPoint = new Point(0, 0);
	Shape line;
	AbstractShape shape;
	AbstractShape selectedShape;
	MouseAdapter m5;
	boolean t = false;
	boolean isCreated = false;
	boolean m1 = false;
	boolean m2 = false;
	int u = 0;
	int numOfShapes = 0;
	int index;
	// ellipse & circle
	int numOfClicks = 0;
	double yR = 0;
	double xR = 0;
	boolean move = false;
	DrawingEngine paint = new Controller();
	List instantShapes = new ArrayList<>();
	int Shape = 0;
	int Shape1 = 0;
	int circleR = 0;
	Point center = new Point(0, 0);
	Point r = new Point(0, 0);
	int px = 0;
	int py = 0;
	int x = 0;
	int y = 0;
	int x2 = 0;
	int y2 = 0;
	int h;
	boolean fill = false;
	Timer timer;
	int pickSelected = 0;
	JPanel selectedColor = new JPanel();
	Color color1 = new Color(0);
	Color color2 = new Color(0);
	Color color3 = new Color(0);
	Color color4 = new Color(0);
	JButton pick1 = new JButton("");
	JButton pick2 = new JButton("");
	JButton pick3 = new JButton("");
	JButton pick4 = new JButton("");
	private JFrame frmPaint;
	MouseAdapter m10;
	Map<String, Double> properties = new HashMap<>();
	boolean fillColor = false;
	int n = 0;
	MouseMotionListener m;
	MouseAdapter m3;
	MouseMotionListener m4;
	MouseAdapter m6;
	int old = 0;
	int ne;
	double fx;
	double fy;
	protected boolean resize = false;
	private MouseAdapter m12;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				
				try {
					DrawSample1 window = new DrawSample1();
					
					window.frmPaint.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * 
	 * @wbp.parser.entryPoint
	 */
	public DrawSample1() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmPaint = new JFrame();
		frmPaint.setTitle("Paint");
		frmPaint.setResizable(false);
		frmPaint.setBounds(100, 100, 807, 589);
		frmPaint.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JPanel panel = new JPanel();
		panel.setBackground(new Color(153, 153, 153));
		panel.setToolTipText("");
		frmPaint.getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);

		
		JPanel panel_1 = new JPanel();
		panel_1.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
			}

		});

		panel_1.setBorder(new BevelBorder(BevelBorder.LOWERED, new Color(255, 165, 0), new Color(255, 165, 0),
				new Color(255, 165, 0), new Color(255, 165, 0)));
		panel_1.setBackground(Color.WHITE);
		Graphics g = panel_1.getGraphics();

		m10 = new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (shape != null || select) {
					reDraw(panel_1);
				}
			}

		};
		JButton btnNewButton = new JButton("Line");
		btnNewButton.setForeground(Color.WHITE);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnNewButton.setBackground(new Color(255, 140, 0));
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				shape = new LineSegment();
				Shape = 1;
				if(Shape1 == 0) {
					reDraw(panel_1);
					Shape1++;
				}
				
			}
		});
		btnNewButton.setFont(new Font("BankGothic Md BT", Font.BOLD, 11));
		btnNewButton.setBounds(694, 11, 95, 23);
		panel.add(btnNewButton);


		JLabel label_5 = new JLabel("");
		label_5.setForeground(new Color(30, 144, 255));
		label_5.setFont(new Font("BankGothic Md BT", Font.PLAIN, 15));
		label_5.setBackground(Color.WHITE);
		label_5.setBounds(673, 535, 31, 26);
		panel.add(label_5);
		selectedColor.setBounds(37, 422, 41, 64);
		panel.add(selectedColor);

		panel_1.setBounds(115, 81, 674, 382);
		panel.add(panel_1);

		JButton btnTriangle = new JButton("Triangle");
		btnTriangle.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				Shape = 2;
				shape = new Triangle();
				if(Shape1 == 0) {
					reDraw(panel_1);
					Shape1++;
				}

			}
		});
		btnTriangle.setForeground(Color.WHITE);
		btnTriangle.setFont(new Font("BankGothic Md BT", Font.BOLD, 11));
		btnTriangle.setBackground(new Color(255, 140, 0));
		btnTriangle.setBounds(694, 45, 95, 23);
		panel.add(btnTriangle);

		JButton btnCircle = new JButton("Circle");

		btnCircle.addMouseListener(new MouseAdapter() {
			@Override

			public void mouseClicked(MouseEvent arg0) {
				Shape = 3;
				shape = new Circle();
				if(Shape1 == 0) {
					reDraw(panel_1);
					Shape1++;
				}

			}
		});
		btnCircle.setForeground(Color.WHITE);
		btnCircle.setFont(new Font("BankGothic Md BT", Font.BOLD, 11));
		btnCircle.setBackground(new Color(255, 140, 0));
		btnCircle.setBounds(584, 11, 97, 23);
		panel.add(btnCircle);

		JButton btnEllipse = new JButton("Ellipse");
		btnEllipse.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				shape = new Ellipse();
				Shape = 4;
				if(Shape1 == 0) {
					reDraw(panel_1);
					Shape1++;
				}
			}
		});
		btnEllipse.setForeground(Color.WHITE);
		btnEllipse.setFont(new Font("BankGothic Md BT", Font.BOLD, 11));
		btnEllipse.setBackground(new Color(255, 140, 0));
		btnEllipse.setBounds(584, 45, 97, 23);
		panel.add(btnEllipse);
		String choosertitle = null;
		JButton btnNewButton_1 = new JButton("Save As .Json");
		btnNewButton_1.setBackground(new Color(255, 140, 0));
		btnNewButton_1.setForeground(new Color(255, 255, 255));
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());

				int returnValue = jfc.showOpenDialog(null);
				// int returnValue = jfc.showSaveDialog(null);

				if (returnValue == JFileChooser.APPROVE_OPTION) {
					File selectedFile = jfc.getSelectedFile();
					paint.save(selectedFile.getAbsolutePath() + ".json");
					Graphics g = panel_1.getGraphics();
					paint.refresh(g);
				}
			}
		});
		btnNewButton_1.setBounds(21, 11, 124, 23);
		panel.add(btnNewButton_1);

		JButton btnNewButton_2 = new JButton("Save As .XML");
		btnNewButton_2.setBackground(new Color(255, 140, 0));
		btnNewButton_2.setForeground(new Color(255, 255, 255));
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());

				int returnValue = jfc.showOpenDialog(null);
				// int returnValue = jfc.showSaveDialog(null);

				if (returnValue == JFileChooser.APPROVE_OPTION) {
					File selectedFile = jfc.getSelectedFile();
					paint.save(selectedFile.getAbsolutePath() + ".xml");
					Graphics g = panel_1.getGraphics();
					paint.refresh(g);
				}
			}
		});
		btnNewButton_2.setBounds(21, 32, 124, 23);
		panel.add(btnNewButton_2);

		JButton btnSquare = new JButton("Square");
		btnSquare.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				shape = new Square();
				shape.type = 5;
				Shape = 5;
				if(Shape1 == 0) {
					reDraw(panel_1);
					Shape1++;
				}
			}
		});
		btnSquare.setForeground(Color.WHITE);
		btnSquare.setFont(new Font("BankGothic Md BT", Font.BOLD, 11));
		btnSquare.setBackground(new Color(255, 140, 0));
		btnSquare.setBounds(466, 11, 108, 23);
		panel.add(btnSquare);

		JButton btnRectangle = new JButton("rectangle");
		btnRectangle.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				shape = new Rectangle();
				Shape = 6;
				if(Shape1 == 0) {
					reDraw(panel_1);
					Shape1++;
				}
			}

			@Override
			public void mousePressed(MouseEvent e) {
				shape = new Rectangle();
				Shape = 6;

			}
		});
		btnRectangle.setForeground(Color.WHITE);
		btnRectangle.setFont(new Font("BankGothic Md BT", Font.BOLD, 11));
		btnRectangle.setBackground(new Color(255, 140, 0));
		btnRectangle.setBounds(466, 45, 108, 23);
		panel.add(btnRectangle);
		JLabel label_2 = new JLabel("");
		label_2.setFont(new Font("BankGothic Md BT", Font.BOLD, 12));
		label_2.setBounds(627, 404, 46, 14);
		panel.add(label_2);
		JSlider slider = new JSlider();
		slider.setValue(0);
		slider.setMaximum(50);
		slider.setMinimum(-50);
		slider.setPaintLabels(true);
		slider.setBackground(new Color(30, 144, 255));
		slider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {

				label_5.setText("" + slider.getValue());
				ne = slider.getValue();

				int diff = ne - old;

				Shape[] ss = paint.getShapes();

				Map<String, Double> p = new HashMap<>();

				 if (shape instanceof Triangle) {
						p = ss[index].getProperties();

						Double x = p.get("endPointx");
						Double x1 = p.get("startPointx");
						Double y1 = p.get("startPointy");
						Double y = p.get("endPointy");
						Double s = p.get("slope1");
						p.put("startPointy", y1);
						p.put("startPointx", x1);

						Double x2 = p.get("secondPointx");
						Double y2 = p.get("secondPointy");
						Double s2 = p.get("slope2");

						if (x + (6 * diff) - x1 > 0 && y + 6 * diff * s - y1 > 0) {
							p.put("endPointx", x + (6 * diff));

							p.put("endPointy", y + 6 * diff * s);

							p.put("secondPointx", x2 + (6 * diff));

							p.put("secondPointy", y2 + 6 * diff * s2);

							old = ne;
							ss[index].setProperties(p);

							Graphics g = panel_1.getGraphics();
							paint.refresh(g);

						}

					} 
				
				 else if (shape instanceof LineSegment) {
					p = ss[index].getProperties();

					Double x = p.get("endPointx");
					Double x1 = p.get("startPointx");
					Double y1 = p.get("startPointy");
					Double y = p.get("endPointy");
					Double s = p.get("slope");
					p.put("startPointy", y1);
					p.put("startPointx", x1);

					if (x + (6 * diff) - x1 > 0 && y + 6 * diff * s - y1 > 0) {
						p.put("endPointx", x + (6 * diff));

						p.put("endPointy", y + 6 * diff * s);

						old = ne;
						ss[index].setProperties(p);

						Graphics g = panel_1.getGraphics();
						paint.refresh(g);

					}

				}
				 else if (shape instanceof Circle) {
						p = ss[index].getProperties();

						Double x = p.get("xAxis");
						Double y = p.get("yAxis");

						Point po = ss[index].getPosition();

						int y1 = po.y;
						int x1 = po.x;

						Double s = p.get("slope");

						if (x + (6 * diff) - x1 > 0 && y + 6 * diff * s - y1 > 0) {
							p.put("xAxis", x + (6 * diff));

							p.put("yAxis", y + 6 * diff * s);
							old = ne;
							ss[index].setProperties(p);

							Graphics g = panel_1.getGraphics();
							paint.refresh(g);

						}

					}

				
				else if (shape instanceof Ellipse) {
					p = ss[index].getProperties();

					Double x = p.get("xAxis");
					Double y = p.get("yAxis");

					Point po = ss[index].getPosition();

					int y1 = po.y;
					int x1 = po.x;

					Double s = p.get("slope");

					if (x + (6 * diff) - x1 > 0 && y + 6 * diff * s - y1 > 0) {
						p.put("xAxis", x + (6 * diff));

						p.put("yAxis", y + 6 * diff * s);
						old = ne;
						ss[index].setProperties(p);

						Graphics g = panel_1.getGraphics();
						paint.refresh(g);

					}

				}

				else if (shape instanceof Rectangle) {
					p = ss[index].getProperties();

					Double x = p.get("endx");
					Double y = p.get("endy");

					Point po = ss[index].getPosition();

					int y1 = po.y;
					int x1 = po.x;

					Double s = p.get("slope");

					if (x + (6 * diff) - x1 > 0 && y + 6 * diff * s - y1 > 0) {
						p.put("endx", x + (6 * diff));

						p.put("endy", y + 6 * diff * s);
						old = ne;
						ss[index].setProperties(p);

						Graphics g = panel_1.getGraphics();
						paint.refresh(g);

					}

				}

			}
			});



		slider.setBounds(577, 487, 200, 26);
		panel.add(slider);

		JLabel lblNewLabel = new JLabel("0");
		lblNewLabel.setFont(new Font("BankGothic Md BT", Font.BOLD, 12));
		lblNewLabel.setBounds(547, 404, 23, 14);
		panel.add(lblNewLabel);

		JLabel label_1 = new JLabel("100");
		label_1.setFont(new Font("BankGothic Md BT", Font.BOLD, 12));
		label_1.setBounds(703, 404, 36, 14);
		panel.add(label_1);

		JButton noColor = new JButton("");
		noColor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				selectedColor.setBackground(noColor.getBackground());

			}
		});
		noColor.setBackground(Color.BLUE);
		noColor.setFont(new Font("Tahoma", Font.PLAIN, 13));
		noColor.setBounds(22, 148, 23, 23);
		panel.add(noColor);

		JButton button = new JButton("");
		button.setBackground(Color.BLACK);
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				selectedColor.setBackground(button.getBackground());

			}
		});
		button.setFont(new Font("Tahoma", Font.PLAIN, 13));
		button.setBounds(65, 114, 23, 23);
		panel.add(button);

		JButton button_1 = new JButton("");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				selectedColor.setBackground(button_1.getBackground());

			}
		});
		button_1.setBackground(Color.WHITE);
		button_1.setFont(new Font("Tahoma", Font.PLAIN, 13));
		button_1.setBounds(22, 114, 23, 23);
		panel.add(button_1);

		JButton button_2 = new JButton("");
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				selectedColor.setBackground(button_2.getBackground());

			}
		});
		button_2.setBackground(Color.RED);
		button_2.setFont(new Font("Tahoma", Font.PLAIN, 13));
		button_2.setBounds(65, 148, 23, 23);
		panel.add(button_2);

		JButton button_3 = new JButton("");
		button_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				selectedColor.setBackground(button_3.getBackground());

			}
		});
		button_3.setFont(new Font("Tahoma", Font.PLAIN, 13));
		button_3.setBackground(Color.PINK);
		button_3.setBounds(22, 182, 23, 23);
		panel.add(button_3);

		JButton button_4 = new JButton("");
		button_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				selectedColor.setBackground(button_4.getBackground());

			}
		});
		button_4.setFont(new Font("Tahoma", Font.PLAIN, 13));
		button_4.setBackground(Color.ORANGE);
		button_4.setBounds(65, 182, 23, 23);
		panel.add(button_4);

		JButton button_5 = new JButton("");
		button_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				selectedColor.setBackground(button_5.getBackground());

			}
		});
		button_5.setFont(new Font("Tahoma", Font.PLAIN, 13));
		button_5.setBackground(Color.YELLOW);
		button_5.setBounds(22, 219, 23, 23);
		panel.add(button_5);

		JButton button_6 = new JButton("");
		button_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				selectedColor.setBackground(button_6.getBackground());
			}
		});
		button_6.setFont(new Font("Tahoma", Font.PLAIN, 13));
		button_6.setBackground(Color.GREEN);
		button_6.setBounds(65, 218, 23, 23);
		panel.add(button_6);

		JButton button_7 = new JButton("");
		button_7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				selectedColor.setBackground(button_7.getBackground());
			}
		});
		button_7.setFont(new Font("Tahoma", Font.PLAIN, 13));
		button_7.setBackground(Color.MAGENTA);
		button_7.setBounds(22, 253, 23, 23);
		panel.add(button_7);

		JButton button_8 = new JButton("");
		button_8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				selectedColor.setBackground(button_8.getBackground());

			}
		});
		button_8.setFont(new Font("Tahoma", Font.PLAIN, 13));
		button_8.setBackground(Color.CYAN);
		button_8.setBounds(65, 252, 23, 23);
		panel.add(button_8);
		pick1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		pick1.setFont(new Font("Tahoma", Font.PLAIN, 13));
		pick1.setBackground(Color.LIGHT_GRAY);
		pick1.setBounds(64, 262, 41, 23);
		panel.add(pick2);
		pick1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				selectedColor.setBackground(pick1.getBackground());

			}
		});
		JLabel error = new JLabel("");
		JButton btnPick = new JButton("pick");
		btnPick.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				{

					Color c = JColorChooser.showDialog(null, "Choose a Color", pick1.getForeground());
					final Graphics g = panel_1.getGraphics();
					paint.refresh(g);
					if (c != null) {
						selectedColor.setBackground(c);

					}

				}
			}
		});
		btnPick.setForeground(Color.WHITE);
		btnPick.setFont(new Font("BankGothic Md BT", Font.PLAIN, 11));
		btnPick.setBackground(new Color(70, 130, 180));
		btnPick.setBounds(23, 287, 68, 23);
		panel.add(btnPick);

		error.setForeground(new Color(139, 0, 0));
		error.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 15));
		error.setBounds(469, 493, 262, 20);
		panel.add(error);
		selectedColor.setBackground(new Color(169, 169, 169));
		selectedColor.setBorder(new LineBorder(Color.BLACK));
		selectedColor.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
			}
		});

		JRadioButton rdbtnFillColor = new JRadioButton("Fill Color");
		rdbtnFillColor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				fillColor = rdbtnFillColor.isSelected();
				if (shape != null) {
					shape.setColor(selectedColor.getBackground());
				}

			}
		});
		rdbtnFillColor.setBackground(new Color(255, 255, 255));
		rdbtnFillColor.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {

			}
		});
		rdbtnFillColor.setForeground(new Color(70, 130, 180));
		rdbtnFillColor.setFont(new Font("BankGothic Md BT", Font.PLAIN, 12));
		rdbtnFillColor.setBounds(6, 317, 103, 23);
		panel.add(rdbtnFillColor);

		JButton btnUndo = new JButton("Undo");
		btnUndo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnUndo.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {

				if (u < 20) {
					Graphics g = panel_1.getGraphics();
					paint.undo();
					paint.refresh(g);
					u++;
				}

			}
		});
		btnUndo.setForeground(Color.WHITE);
		btnUndo.setFont(new Font("BankGothic Md BT", Font.PLAIN, 11));
		btnUndo.setBackground(new Color(70, 130, 180));
		btnUndo.setBounds(369, 11, 68, 23);
		panel.add(btnUndo);

		JButton btnRedo = new JButton("Redo");
		btnRedo.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				if (u > 0) {
					Graphics g = panel_1.getGraphics();
					paint.redo();
					paint.refresh(g);
					u--;
				}
				

			}
		});
		btnRedo.setForeground(Color.WHITE);
		btnRedo.setFont(new Font("BankGothic Md BT", Font.PLAIN, 11));
		btnRedo.setBackground(new Color(70, 130, 180));
		btnRedo.setBounds(369, 44, 68, 23);
		panel.add(btnRedo);
		selectedColor.setBounds(37, 422, 41, 37);
		panel.add(selectedColor);
		JLabel lblNewLabel_1 = new JLabel("Resize");
		lblNewLabel_1.setForeground(new Color(30, 144, 255));
		lblNewLabel_1.setFont(new Font("BankGothic Md BT", Font.PLAIN, 15));
		lblNewLabel_1.setBackground(new Color(255, 255, 255));
		lblNewLabel_1.setBounds(650, 515, 54, 26);
		panel.add(lblNewLabel_1);

		JLabel label_4 = new JLabel("+");
		label_4.setForeground(new Color(30, 144, 255));
		label_4.setFont(new Font("BankGothic Md BT", Font.PLAIN, 23));
		label_4.setBackground(Color.WHITE);
		label_4.setBounds(758, 506, 31, 38);
		panel.add(label_4);

		JLabel label_3 = new JLabel("-");
		label_3.setForeground(new Color(30, 144, 255));
		label_3.setFont(new Font("BankGothic Md BT", Font.PLAIN, 27));
		label_3.setBackground(Color.WHITE);
		label_3.setBounds(587, 503, 46, 44);
		panel.add(label_3);

		JButton select_1 = new JButton("Move");
		select_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				if (select) {
					select = false;
					select_1.setBackground(Color.gray);
					panel_1.removeMouseListener(m10);
					panel_1.addMouseListener(m3);
				} else {
					select = true;
					select_1.setBackground(Color.RED);
					panel_1.addMouseListener(m10);
					panel_1.removeMouseListener(m3);

				}
				System.out.println(select);
			}
		});

		JButton btnLoad = new JButton("load");
		btnLoad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
				jfc.showOpenDialog(null);
				
				File selectedFile = jfc.getSelectedFile();
				paint.load(selectedFile.getAbsolutePath());
				Graphics g = panel_1.getGraphics();
				paint.refresh(g);
				
				

			}
		});
		btnLoad.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {

			}
		});
		btnLoad.setForeground(Color.WHITE);
		btnLoad.setFont(new Font("BankGothic Md BT", Font.PLAIN, 14));
		btnLoad.setBackground(new Color(70, 130, 180));
		btnLoad.setBounds(21, 359, 76, 37);
		panel.add(btnLoad);
		select_1.setForeground(Color.WHITE);
		select_1.setFont(new Font("BankGothic Md BT", Font.PLAIN, 11));
		select_1.setBackground(new Color(70, 130, 180));
		select_1.setBounds(173, 11, 68, 23);
		panel.add(select_1);

		JRadioButton rdbtnMove = new JRadioButton("Move");
		rdbtnMove.setForeground(new Color(30, 144, 255));
		rdbtnMove.setFont(new Font("BankGothic Md BT", Font.BOLD, 11));
		rdbtnMove.setBackground(Color.WHITE);
		rdbtnMove.addPropertyChangeListener(new PropertyChangeListener() {
			public void propertyChange(PropertyChangeEvent arg5) {
				if (move) {
					move = false;
					panel_1.removeMouseListener(m5);
					panel_1.removeMouseMotionListener(m4);
					panel_1.removeMouseListener(m6);

				} else {
					panel_1.removeMouseListener(m3);
					m4 = new MouseMotionAdapter() {
						@Override
						public void mouseDragged(MouseEvent e) {
							if (select) {
								panel_1.removeMouseListener(m3);
								if (selectedShape.type == 6) {
									r.x = e.getX();
									r.y = e.getY();
									properties = selectedShape.getProperties();
									Point l2 = selectedShape.getPosition();
									double xselect = properties.get("endx") - l2.x;
									double yselect = properties.get("endy") - l2.y;
									properties = selectedShape.getProperties();
									properties.replace("endx", l2.x + xselect);
									properties.replace("endy", l2.y + yselect);
									Point l = new Point(selectPoint.x + r.x, selectPoint.y + r.y);
									selectedShape.setPosition(l);
									properties = selectedShape.getProperties();
									properties.replace("endy", yselect + l.y);
									properties.replace("endx", xselect + l.x);
									shape.setProperties(properties);
									panel_1.removeMouseListener(m3);
									Graphics g = panel_1.getGraphics();
									paint.refresh(g);
								}
							}
							if (selectedShape.type == 5) {
								r.x = e.getX();
								r.y = e.getY();
								properties = selectedShape.getProperties();
								Point l2 = selectedShape.getPosition();
								double xselect = properties.get("endx") - l2.x;
								double yselect = properties.get("endy") - l2.y;
								properties = selectedShape.getProperties();
								properties.replace("endx", l2.x + xselect);
								properties.replace("endy", l2.y + yselect);
								Point l = new Point(selectPoint.x + r.x, selectPoint.y + r.y);
								selectedShape.setPosition(l);
								properties = selectedShape.getProperties();
								properties.replace("endy", yselect + l.y);
								properties.replace("endx", xselect + l.x);
								shape.setProperties(properties);
								panel_1.removeMouseListener(m3);
								Graphics g = panel_1.getGraphics();
								paint.refresh(g);
							}

							if (selectedShape.type == 4) {
								r.x = e.getX();
								r.y = e.getY();
								properties = selectedShape.getProperties();
								Point l2 = selectedShape.getPosition();
								double xselect = properties.get("xAxis") - l2.x;
								double yselect = properties.get("yAxis") - l2.y;
								properties = selectedShape.getProperties();
								properties.replace("xAxis", l2.x + xselect);
								properties.replace("yAxis", l2.y + yselect);
								Point l = new Point(selectPoint.x + r.x, selectPoint.y + r.y);
								selectedShape.setPosition(l);
								properties = selectedShape.getProperties();
								properties.replace("yAxis", yselect + l.y);
								properties.replace("xAxis", xselect + l.x);
								shape.setProperties(properties);
								panel_1.removeMouseListener(m3);
								Graphics g = panel_1.getGraphics();
								paint.refresh(g);
							}
							if (selectedShape.type == 3) {
								r.x = e.getX();
								r.y = e.getY();
								properties = selectedShape.getProperties();
								Point l2 = selectedShape.getPosition();
								double xselect = properties.get("xAxis") - l2.x;
								double yselect = properties.get("yAxis") - l2.y;
								properties = selectedShape.getProperties();
								properties.replace("xAxis", l2.x + xselect);
								properties.replace("yAxis", l2.y + yselect);
								Point l = new Point(selectPoint.x + r.x, selectPoint.y + r.y);
								selectedShape.setPosition(l);
								properties = selectedShape.getProperties();
								properties.replace("yAxis", yselect + l.y);
								properties.replace("xAxis", xselect + l.x);
								shape.setProperties(properties);
								panel_1.removeMouseListener(m3);
								Graphics g = panel_1.getGraphics();
								paint.refresh(g);
							}
							if (selectedShape.type == 2) {
								r.x = e.getX();
								r.y = e.getY();
								properties = selectedShape.getProperties();
								Point l2 = selectedShape.getPosition();
								double l2x = ((Triangle) selectedShape).x;
								double l2y = ((Triangle) selectedShape).y;
								double x2select = ((Triangle) selectedShape).x2 - l2x;
								double x3select = ((Triangle) selectedShape).x3 - l2x;
								double y2select = ((Triangle) selectedShape).y2 - l2y;
								double y3select = ((Triangle) selectedShape).y3 - l2y;
								((Triangle) selectedShape).x2 = l2x - x2select;
								((Triangle) selectedShape).x3 = l2x - x3select;
								((Triangle) selectedShape).y2 = l2y - y2select;
								((Triangle) selectedShape).y3 = l2y - y3select;
								Point l = new Point(selectPoint.x + r.x, selectPoint.y + r.y);
								((Triangle) selectedShape).x = (double) l.x;
								((Triangle) selectedShape).y = (double) l.y;
								((Triangle) selectedShape).x2 = l.x + x2select;
								((Triangle) selectedShape).x3 = l.x + x3select;
								((Triangle) selectedShape).y2 = l.y + y2select;
								((Triangle) selectedShape).y3 = l.y + y3select;
								panel_1.removeMouseListener(m3);
								Graphics g = panel_1.getGraphics();
								paint.refresh(g);
							}
							if (selectedShape.type == 1) {
								panel_1.addMouseListener(m5);
								r.x = e.getX();
								r.y = e.getY();
								properties = selectedShape.getProperties();
								Point l2 = selectedShape.getPosition();
								double l2x = properties.get("startPointx");
								double l2y = properties.get("startPointy");
								double xselect = properties.get("endPointx") - l2x;
								double yselect = properties.get("endPointy") - l2y;
								properties = selectedShape.getProperties();
								properties.replace("endPointx", l2x + xselect);
								properties.replace("endPointy", l2y + yselect);
								Point l = new Point(selectPoint.x + r.x, selectPoint.y + r.y);
								selectedShape.setPosition(l);
								properties.replace("startPointy", (double) l.y);
								properties.replace("startPointx", (double) l.x);
								properties = selectedShape.getProperties();
								properties.replace("endPointy", yselect + l.y);
								properties.replace("endPointx", xselect + l.x);
								shape.setProperties(properties);
								panel_1.removeMouseListener(m3);
								Graphics g = panel_1.getGraphics();
								paint.addShape(selectedShape);
								paint.removeShape(shape);
								shape.color = selectedShape.color;
								paint.refresh(g);
							}
						}

					};
					m5 = new MouseAdapter() {
						@Override
						public void mouseReleased(MouseEvent arg1) {
							panel_1.removeMouseMotionListener(m4);
							
							for (int i = 0; i < paint.getShapes().length; i++) {
								if (paint.getShapes()[i].equals(selectedShape)) {
									index = i;
									System.out.println("shape is ready to resize");
									break;
								}

							}
							panel_1.removeMouseListener(m6);
							panel_1.removeMouseListener(m5);
							panel_1.removeMouseListener(m3);
							Point l2 = null;
							paint.removeShape(selectedShape);
							if (selectedShape.type != 1) {
								newShape(selectedShape);
								paint.removeShape(selectedShape);
							} else {
								newShape(selectedShape);
								paint.removeShape(selectedShape);
								paint.removeShape(shape);

							}
							for(int i=0;i<paint.getShapes().length;i++){
								if(paint.getShapes()[i].getProperties().isEmpty()||((AbstractShape)paint.getShapes()[i]).type==0)
								{
									paint.removeShape(paint.getShapes()[i]);
									
								}
							}
						}

					};

					move = true;

				}
			}
		});
		rdbtnMove.setBounds(173, 46, 68, 23);
		panel.add(rdbtnMove);

		JButton btnResize = new JButton("Resize");
		btnResize.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (resize) {
					resize = false;
					btnResize.setBackground(Color.gray);
					panel_1.removeMouseListener(m12);
					panel_1.addMouseListener(m10);
				} else {
					resize = true;
					btnResize.setBackground(Color.RED);
					panel_1.addMouseListener(m12);
					System.out.println("shape ");
					panel_1.removeMouseListener(m10);

				}
				System.out.println(resize);

			}
		});
		m12 = new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg1) {
				panel_1.removeMouseMotionListener(m4);
				panel_1.removeMouseListener(m6);
				selectedShape = (AbstractShape) paint.select(arg1.getPoint());
				
				for (int i = 0; i < paint.getShapes().length; i++) {
					
					if (paint.getShapes()[i].equals(selectedShape)) {

						index = i;
						System.out.println(index);
						
						break;
					}
					

				}
		        panel_1.requestFocus(); 
				panel_1.addKeyListener(new KeyAdapter() {
					@Override
					public void keyPressed(KeyEvent e) {

				    	paint.removeShape(paint.getShapes()[index]);
						System.out.println("shape is ready to resize.");
				    	Graphics g = panel_1.getGraphics();
				    	paint.refresh(g);
					}
				});

	
	
			}

		};
		btnResize.setForeground(Color.WHITE);
		btnResize.setFont(new Font("BankGothic Md BT", Font.PLAIN, 11));
		btnResize.setBackground(new Color(70, 130, 180));
		btnResize.setBounds(251, 11, 89, 23);
		panel.add(btnResize);

		JLabel selectedColor_1 = new JLabel("");
		selectedColor_1.setFont(new Font("BankGothic Md BT", Font.PLAIN, 11));
		selectedColor_1.setLabelFor(btnNewButton);
		selectedColor_1.setIcon(new ImageIcon("800px_COLOURBOX27554697.jpg"));
		selectedColor_1.setBounds(0, 0, 814, 573);
		panel.add(selectedColor_1);

		JLabel label = new JLabel("");

		label.setIcon(new ImageIcon("800px_COLOURBOX27554697.jpg"));
		label.setBounds(0, 0, 814, 573);
		panel.add(label);

	}

	public void reDraw(JPanel panel_1) {
		((Controller)paint).x=1;
		if (select) {
			int h = 0;
			h++;

		}
		for(int i=0;i<paint.getShapes().length;i++){
			if(((AbstractShape)paint.getShapes()[i]).type==0)
			{
				paint.removeShape(paint.getShapes()[i]);
				
			}
		}
		panel_1.removeMouseListener(m10);
		panel_1.setBackground(Color.WHITE);
		if (!select) {
			shape.setColor(selectedColor.getBackground());

			m = new MouseMotionAdapter() {

				@Override
				public void mouseDragged(MouseEvent e) {
					panel_1.setBackground(Color.WHITE);
					if (!select) {
						shape.setColor(selectedColor.getBackground());
						if (fillColor) {
							shape.setFillColor(selectedColor.getBackground());
						}
						if (Shape == 1) {
							shape.type = 1;
							if (numOfClicks == 1) {
								r.x = e.getX();
								r.y = e.getY();
								properties.put("endPointx", (double) r.x);
								properties.put("endPointy", (double) r.y);
								shape.setProperties(properties);
								Graphics g = panel_1.getGraphics();
								paint.addShape(shape);
								paint.refresh(g);
								paint.removeShape(shape);

							}
						}
						if (Shape == 2) {
							shape.type = 2;
							if (numOfClicks == 1) {
								r.x = e.getX();
								r.y = e.getY();
								properties.put("secondPointx", (double) r.x);
								properties.put("secondPointy", (double) r.y);
								properties.put("endPointx", (double) r.x);
								properties.put("endPointy", (double) r.y);
								shape.setProperties(properties);
								Graphics g = panel_1.getGraphics();
								paint.addShape(shape);
								paint.refresh(g);
								paint.removeShape(shape);

							}
						}
						if (Shape == 3) {
							shape.type = 3;
							if (numOfClicks == 1) {
								r.x = e.getX();
								r.y = e.getY();
								xR = r.distance(center.x, center.y);
								properties.put("xAxis", (double) r.x);
								properties.put("yAxis", (double) r.y);
								shape.setProperties(properties);
								Graphics g = panel_1.getGraphics();
								paint.addShape(shape);
								paint.refresh(g);
								paint.removeShape(shape);

							}

						}
						if (Shape == 4) {
							shape.type = 4;
							if (numOfClicks == 1) {
								r.x = e.getX();
								r.y = e.getY();
								xR = r.distance(center.x, center.y);
								properties.put("xAxis", (double) r.x);
								properties.put("yAxis", (double) r.y);
								shape.setProperties(properties);
								Graphics g = panel_1.getGraphics();
								paint.addShape(shape);
								paint.refresh(g);
								paint.removeShape(shape);

							}

						}
						if (Shape == 5) {
							shape.type = 5;
							if (numOfClicks == 1) {
								r.x = e.getX();
								r.y = e.getY();
								xR = r.distance(center.x, center.y);
								properties.put("endx", (double) r.x);
								properties.put("endy", (double) r.y);
								shape.setProperties(properties);
								Graphics g = panel_1.getGraphics();
								paint.addShape(shape);
								paint.refresh(g);
								paint.removeShape(shape);
							}
						}
						if (Shape == 6) {
							shape.type = 6;
							if (numOfClicks == 1) {
								r.x = e.getX();
								r.y = e.getY();
								xR = r.distance(center.x, center.y);
								properties.put("endx", (double) r.x);
								properties.put("endy", (double) r.y);
								shape.setProperties(properties);
								Graphics g = panel_1.getGraphics();
								paint.addShape(shape);
								paint.refresh(g);
								paint.removeShape(shape);
							}
						}
					}

				}
			};

			MouseMotionListener m1 = new MouseMotionAdapter() {
				@Override
				public void mouseMoved(MouseEvent e) {

					r.x = e.getX();
					r.y = e.getY();
					properties.put("endPointx", (double) r.x);
					properties.put("endPointy", (double) r.y);
					shape.setProperties(properties);
					Graphics g = panel_1.getGraphics();
					paint.addShape(shape);
					paint.refresh(g);
					paint.removeShape(shape);

				}
			};
			MouseAdapter m2 = new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent arg1) {
					numOfClicks = 0;
					paint.removeShape(shape);
					panel_1.removeMouseMotionListener(m1);
					shape = new Triangle();

					if (n > 6) {
						System.out.println();
					}

				}

			};

			m3 = new MouseAdapter() {
				@Override
				public void mouseReleased(MouseEvent arg1) {
					for(int i=0;i<paint.getShapes().length;i++){
						if(((AbstractShape)paint.getShapes()[i]).type==0)
						{
							paint.removeShape(paint.getShapes()[i]);
							
						}}
					paint.addShape(shape);
					numOfShapes++;
					
					if (Shape == 1) {
						shape = new LineSegment();

					} else if (Shape == 2) {

						panel_1.addMouseMotionListener(m1);
						panel_1.addMouseListener(m2);

					} 
					else if (Shape == 3) {
						shape = new Circle();
						center = new Point(0, 0);
					} 
					else if (Shape == 4) {
						shape = new Ellipse();
						center = new Point(0, 0);
					} else if (Shape == 5) {
						shape.type = 5;
						shape = new Square();
						center = new Point(0, 0);
					} else if (Shape == 6) {
						shape = new Rectangle();
						center = new Point(0, 0);
					}

					numOfClicks = 0;
					panel_1.removeMouseMotionListener(m);

				}

				@Override
				public void mousePressed(MouseEvent arg0) {
					panel_1.setBackground(Color.WHITE);
					if (Shape == 1) {
						if (numOfClicks == 0) {

							r.x = arg0.getX();
							r.y = arg0.getY();

							numOfClicks = 1;
							properties = new HashMap<>();
							properties.put("startPointx", (double) r.x);
							properties.put("startPointy", (double) r.y);

							panel_1.addMouseMotionListener(m);
						}

					}
					if (Shape == 2) {
						if (numOfClicks == 0) {
							n++;
							r.x = arg0.getX();
							r.y = arg0.getY();

							numOfClicks = 1;
							properties = new HashMap<>();
							properties.put("startPointx", (double) r.x);
							properties.put("startPointy", (double) r.y);

							panel_1.addMouseMotionListener(m);
						}

					}
					if (Shape == 3) {
						shape.type = 3;
						if (numOfClicks == 0) {

							numOfClicks = 1;

							center.x = arg0.getX();
							center.y = arg0.getY();
							yR = 5;

							shape.setPosition(center);

							panel_1.addMouseMotionListener(m);

						}

					}
					if (Shape == 4) {
						if (numOfClicks == 0) {

							numOfClicks = 1;

							center.x = arg0.getX();
							center.y = arg0.getY();
							yR = 5;

							shape.setPosition(center);

							panel_1.addMouseMotionListener(m);

						}
					}

					if (Shape == 5) {
						if (numOfClicks == 0) {

							numOfClicks = 1;
							center.x = arg0.getX();
							center.y = arg0.getY();

							shape.setPosition(center);
							panel_1.addMouseMotionListener(m);

						}

					}

					if (Shape == 6) {
						if (numOfClicks == 0) {
							numOfClicks = 1;
							center.x = arg0.getX();
							center.y = arg0.getY();
							yR = 5;

							shape.setPosition(center);
							panel_1.addMouseMotionListener(m);

						}

					}
					if (Shape == 7) {

					}

				}
			};
			panel_1.addMouseListener(m3);

		} else {
			panel_1.removeMouseListener(m3);

			m6 = new MouseAdapter() {
				@Override
				public void mousePressed(MouseEvent arg0) {
					int ij=paint.getShapes().length;
for(int i=0;i<paint.getShapes().length;i++){
	if(paint.getShapes()[i].getProperties().isEmpty())
	{
		paint.removeShape(paint.getShapes()[i]);
		
	}
}
					selectedShape = (AbstractShape) paint.select(arg0.getPoint());
					for (int i = 0; i < paint.getShapes().length; i++) {
						if (paint.getShapes()[i].equals(selectedShape)) {
							index = i;
							break;

						}
					}
					if (move) {
						panel_1.removeMouseListener(m3);
						if (selectedShape != null && select) {
							selectPoint.x = selectedShape.getPosition().x - arg0.getX();
							selectPoint.y = selectedShape.getPosition().y - arg0.getY();
							if (selectedShape.type == 2) {
								selectedShape = (Triangle) selectedShape;
								selectPoint.x = (int) (((Triangle) selectedShape).x - arg0.getX());
								selectPoint.y = (int) (((Triangle) selectedShape).y - arg0.getY());
							}
							if (selectedShape.type == 1) {
								selectedShape = (LineSegment) selectedShape;
								selectPoint.x = (int) (((LineSegment) selectedShape).x - arg0.getX());
								selectPoint.y = (int) (((LineSegment) selectedShape).y - arg0.getY());
							}
							panel_1.addMouseListener(m5);
							panel_1.addMouseMotionListener(m4);
						}
					}
					int i2 = 0;
					i2 = i2 + 1;
					return;
				}
			};
			panel_1.addMouseListener(m6);

		}

	}

	public void newShape(AbstractShape x) {
		if (x.type == 6) {
			AbstractShape v = new Rectangle();
			Map<String, Double> properties = new HashMap<>();
			properties.put("endy", x.getProperties().get("endy"));
			properties.put("endx", x.getProperties().get("endx"));
			v.setPosition(x.getPosition());
			v.type=6;
			v.setColor(x.getColor());
			if (x.getFillColor() != null)
				v.setFillColor(x.getFillColor());
			v.setProperties(properties);
			paint.addShape(v);
		}
		if (x.type == 5) {
			AbstractShape v = new Square();
			Map<String, Double> properties = new HashMap<>();
			properties.put("endy", x.getProperties().get("endy"));
			properties.put("endx", x.getProperties().get("endx"));
			v.setPosition(x.getPosition());
			v.type=5;
			
			v.setColor(x.getColor());
			if (x.getFillColor() != null)
				v.setFillColor(x.getFillColor());
			v.setProperties(properties);
			paint.addShape(v);
		}
		if (x.type == 3) {
			AbstractShape v = new Circle();
			Map<String, Double> properties = new HashMap<>();
			properties.put("xAxis", x.getProperties().get("xAxis"));
			properties.put("yAxis", x.getProperties().get("yAxis"));
			v.setPosition(x.getPosition());
			v.setColor(x.getColor());
			v.type=3;
			if (x.getFillColor() != null)
				v.setFillColor(x.getFillColor());
			v.setProperties(properties);
			paint.addShape(v);
			paint.removeShape(x);
		}
		if (x.type == 4) {
			AbstractShape v = new Ellipse();
			Map<String, Double> properties = new HashMap<>();
			properties.put("xAxis", x.getProperties().get("xAxis"));
			properties.put("yAxis", x.getProperties().get("yAxis"));
			v.setPosition(x.getPosition());
			v.setColor(x.getColor());
			v.type=4;
			if (x.getFillColor() != null)
				v.setFillColor(x.getFillColor());
			v.setProperties(properties);
			paint.addShape(v);
			paint.removeShape(x);
		}
		if (x.type == 2) {
			AbstractShape v = new Triangle();
			Map<String, Double> properties = new HashMap<>();
			properties.put("startPointx", ((Triangle) x).x);
			properties.put("startPointy", ((Triangle) x).y);
			properties.put("secondPointx", ((Triangle) x).x2);
			properties.put("secondPointy", ((Triangle) x).y2);
			properties.put("endPointx", ((Triangle) x).x3);
			properties.put("endPointy", ((Triangle) x).y3);
			v.setPosition(x.getPosition());
			v.setColor(x.getColor());
			v.type=2;
			if (x.getFillColor() != null)
				v.setFillColor(x.getFillColor());
			v.setProperties(properties);
			paint.addShape(v);
			
		}

		if (x.type == 1) {
			AbstractShape v = new LineSegment();
			Map<String, Double> properties = new HashMap<>();
			properties.put("startPointx", ((LineSegment) x).x);
			properties.put("startPointy", ((LineSegment) x).y);
			properties.put("endPointx", ((LineSegment) x).x2);
			properties.put("endPointy", ((LineSegment) x).y2);
			v.setPosition(x.getPosition());
			v.setColor(x.getColor());
			v.type=1;
			if (x.getFillColor() != null)
				v.setFillColor(x.getFillColor());
			v.setProperties(properties);
			paint.removeShape(x);
			paint.addShape(v);
		}

	}
}