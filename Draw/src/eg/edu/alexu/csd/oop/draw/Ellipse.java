package eg.edu.alexu.csd.oop.draw;

import java.awt.Graphics;
import java.awt.Point;
import java.util.HashMap;
import java.util.Map;

public class Ellipse extends AbstractShape{
	
Map<String, Double> properties = new HashMap<>();
	
	@Override
	public void setProperties(Map<String, Double> properties) {
		// TODO Auto-generated method stub
		
		Double x1= properties.get("xAxis");
		this.properties.put("xAxis", x1);
		Double y1= properties.get("yAxis");
		this.properties.put("yAxis", y1);
	}
	@Override
	public Map<String, Double> getProperties() {
		// TODO Auto-generated method stub
		return properties;
	}
	public boolean isSelected(Point screen) {
		int x = (int) Math.round(properties.get("xAxis"));
        int y = (int) Math.round(properties.get("yAxis"));
        
        
        Point p = new Point();
        p = getPosition();
		if(screen.x<Math.max(x, p.x)&&screen.y<Math.max(y, p.y)&&screen.x>Math.min(x, p.x)&&screen.y>Math.min(y, p.y))
		{
			
			return true;
			
		}
		
		return false;

	}	
        @Override
	public void draw(Graphics canvas) {
		// TODO Auto-generated method stub
        	if(!properties.isEmpty()) {
           int x = (int) Math.round(properties.get("xAxis")); // 3
           int y = (int) Math.round(properties.get("yAxis"));
           int xs;
           int ys;
           Point p = new Point();
           p = getPosition();
           canvas.setColor(this.color);
           if(x<p.x&&y<p.y){
        	   xs=p.x;
        	   ys=p.y;
        	   if(this.fill)
               canvas.fillOval(x , y , xs-x, ys-y);
        	   else 
                   canvas.drawOval(x , y , xs-x, ys-y);

           }
           else if(x<p.x&&y>p.y){
        	   if(this.fill)
           canvas.fillOval(x , p.y , p.x-x, y-p.y);
        	   else    
                   canvas.drawOval(x , p.y , p.x-x, y-p.y);

           }
           else if(x>p.x&&y<p.y){
        	   if(this.fill)
               canvas.fillOval(p.x , y , x-p.x, p.y-y);
        	   else    
                   canvas.drawOval(p.x , y , x-p.x, p.y-y);

           }
           else if(x>p.x&&y>p.y){
        	   if(this.fill)
               canvas.fillOval(p.x , p.y , x-p.x, y-p.y);
        	   else
                   canvas.drawOval(p.x , p.y , x-p.x, y-p.y);

           }
  }
        }

}
