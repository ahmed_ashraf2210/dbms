package eg.edu.alexu.csd.oop.draw;

import java.awt.Graphics;
import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LineSegment extends AbstractShape{

Map<String, Double> properties = new HashMap<>();
	

	Double x;
	Double y;
	Double x2;
	Double y2;
	public List<Shape> record = new ArrayList<>();
	
	
	
	@Override
	public void setProperties(Map<String, Double> properties) {
		// TODO Auto-generated method stub
		
		
		x = properties.get("startPointx");
		this.properties.put("startPointx", x);
		
		y = properties.get("startPointy");
		this.properties.put("startPointy", y);
		
		
		x2 = properties.get("endPointx");
		this.properties.put("endPointx", x2);
		
		y2 = properties.get("endPointy");
		this.properties.put("endPointy", y2);
		
		
		

	}
	public boolean isSelected(Point screen) {
		
        Point p = new Point();
        p = getPosition();
		if(screen.x<Math.max(x, x2)&&screen.y<Math.max(y, y2)&&screen.x>Math.min(x, x2)&&screen.y>Math.min(y, y2))
		{
			
			return true;
			
		}
		
		return false;

	}	
	
	@Override
	public Map<String, Double> getProperties() {
		// TODO Auto-generated method stub
		return properties;
	}
        @Override
	public void draw(Graphics canvas) {
		// TODO Auto-generated method stub
        	if(!properties.isEmpty()){
        		int a = (int) (x - 0);
            	int b = (int) (y - 0);
            	int a1 = (int) (x2 - 0);
            	int b1 = (int) (y2 - 0);
              	 canvas.setColor(this.getColor());
            	canvas.drawLine(a, b, a1, b1);
        	}
        	
        	
        	
        	
 
  }

}
