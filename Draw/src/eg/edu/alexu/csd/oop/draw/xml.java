package eg.edu.alexu.csd.oop.draw;

import java.awt.Color;
import java.awt.Point;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import eg.edu.alexu.csd.oop.test.DummyShape;

public class xml {
public void write(Shape [] n,String path){
		
    	PrintWriter writer = null;
		try {
			writer = new PrintWriter(path, "UTF-8");
		} catch (FileNotFoundException
				| UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		writer.println("<paint>");

int h=0;
for(int i=0;i<n.length;i++)
{				
	writer.print(" <shape id=\"eg.edu.alexu.csd.oop.draw.");
if(n[i] instanceof LineSegment){
writer.print("LineSegment\">");
writer.println();
writer.print("  <x>"+((LineSegment)n[i]).getProperties().get("startPointx")+"</x>");
writer.println();
writer.print("  <y>"+((LineSegment)n[i]).getProperties().get("startPointy")+"</y>");
writer.println();
writer.println("  <map>");
writer.println("   <endPointx>"+((LineSegment)n[i]).getProperties().get("endPointx")+"<endPointx>");
writer.println("   <endPointy>"+((LineSegment)n[i]).getProperties().get("endPointy")+"<endPointy>");
writer.println("  </map>");
writer.println("  <color>"+((LineSegment)n[i]).getColor()+"</color>");
writer.println(" </shape>");
}
if(n[i] instanceof Triangle){
writer.print("Triangle\">");
writer.println();
writer.print("  <x>"+((Triangle)n[i]).x+"</x>");
writer.println();
writer.print("  <y>"+((Triangle)n[i]).y+"</y>");
writer.println();
writer.println("  <map>");
writer.println("   <secondPointx>"+((Triangle)n[i]).x2+"</secondPointx>");
writer.println("   <secondPointy>"+((Triangle)n[i]).y2+"</secondPointy>");
writer.println("   <endPointx>"+((Triangle)n[i]).x3+"</endPointx>");
writer.println("   <endPointy>"+((Triangle)n[i]).y3+"</endPointy>");
writer.println("  </map>");
writer.println("  <color>"+((Triangle)n[i]).color+"</color>");
writer.println("  <fill>"+((Triangle)n[i]).fill+"</fill>");
writer.println(" </shape>");
}
if(n[i] instanceof Circle){
writer.print("Circle");
}
if(n[i] instanceof Ellipse){
	writer.print("Ellipse\">");
	writer.println();
	writer.print("  <x>"+((Ellipse)n[i]).getPosition().x+"</x>");
	writer.println();
	writer.print("  <y>"+((Ellipse)n[i]).getPosition().y+"</y>");
	writer.println();
	writer.println("  <map>");
	writer.println("   <xAxis>"+((Ellipse)n[i]).getProperties().get("xAxis")+"</xAxis>");
	writer.println("   <yAxis>"+((Ellipse)n[i]).getProperties().get("yAxis")+"</yAxis>");
	writer.println("  </map>");
	writer.println("  <color>"+((Ellipse)n[i]).color+"</color>");
	writer.println("  <fill>"+((Ellipse)n[i]).fill+"</fill>");
	writer.println(" </shape>");
}
if(n[i] instanceof Rectangle){
	writer.print("Rectangle\">");
	writer.println();
	writer.print("  <x>"+((Rectangle)n[i]).getPosition().x+"</x>");
	writer.println();
	writer.print("  <y>"+((Rectangle)n[i]).getPosition().y+"</y>");
	writer.println();
	writer.println("  <map>");
	writer.println("   <endx>"+((Rectangle)n[i]).getProperties().get("endx")+"</endx>");
	writer.println("   <endy>"+((Rectangle)n[i]).getProperties().get("endy")+"</endy>");
	writer.println("  </map>");
	writer.println("  <color>"+((Rectangle)n[i]).color+"</color>");
	writer.println("  <fill>"+((Rectangle)n[i]).fill+"</fill>");
	writer.println(" </shape>");
}
if(n[i] instanceof Square){
	writer.print("Square\">");
	writer.println();
	writer.print("  <x>"+((Square)n[i]).getPosition().x+"</x>");
	writer.println();
	writer.print("  <y>"+((Square)n[i]).getPosition().y+"</y>");
	writer.println();
	writer.println("  <map>");
	writer.println("   <endx>"+((Square)n[i]).getProperties().get("endx")+"</endx>");
	writer.println("   <endy>"+((Square)n[i]).getProperties().get("endy")+"</endy>");
	writer.println("  </map>");
	writer.println("  <color>"+((Square)n[i]).color+"</color>");
	writer.println("  <fill>"+((Square)n[i]).fill+"</fill>");
	writer.println(" </shape>");
}
if(n[i] instanceof RoundRectangle){
	writer.print("RoundRectangle\">");
	writer.println();
	writer.print("  <x>"+"-1"+"</x>");
	writer.println();
	writer.print("  <y>"+"-1"+"</y>");
	writer.println();
	writer.println("  <map>");
	writer.println("   <Length>"+((RoundRectangle)n[i]).getProperties().get("Length")+"</Length>");
	writer.println("   <ArcWidth>"+((RoundRectangle)n[i]).getProperties().get("ArcWidth")+"</ArcWidth>");
	writer.println("   <ArcLength>"+((RoundRectangle)n[i]).getProperties().get("ArcLength")+"</ArcLength>");
	writer.println("   <Width>"+((RoundRectangle)n[i]).getProperties().get("Width")+"</Width>");
	writer.println("  </map>");
	writer.println("  <color>"+((RoundRectangle)n[i]).getColor()+"</color>");
	writer.println("  <fill>"+((RoundRectangle)n[i]).getFillColor()+"</fill>");
	writer.println(" </shape>");
}
}
writer.println("]");
writer.println("}");
    	writer.close();


}

	
	
	
	
	
	public List<Shape> read(String path) throws Exception{
		File f = new File(path);

		BufferedReader br = new BufferedReader(new FileReader(f));
		List<Shape> shapes = new ArrayList<>();
		Shape shape;
		Map<String, Double> properties = new HashMap<>();

		String s;
		while ((s = br.readLine()) != null) {
			String shapeName = "a";
			String regex = "( <shape id=\"eg.edu.alexu.csd.oop.draw.)([a-zA-Z]*)";
			Pattern r = Pattern.compile(regex);
			Matcher match = r.matcher(s);

			if (match.find()) {
				shapeName = match.group(2);

			} else {
				System.out.println("Not Found");
			}
			
			switch (shapeName) {
			case "LineSegment":
				String x = null;
				String y = null;
				String endPointx = null;
				String endPointy = null;
				String color = null;
				int c1 = 0;
				int c2 = 0;
				int c3 = 0;

				Double x1;
				Double y1;
				Double endPointx1;
				Double endPointy1;

				shape = new LineSegment();
				properties = new HashMap<>();

				for (int i = 0; i < 7; i++) {
					s = br.readLine();

					String regex1 = "(  <x>)(\\d*\\.\\d*)";
					Pattern r1 = Pattern.compile(regex1);
					Matcher match1 = r1.matcher(s);

					if (match1.find()) {
						x = match1.group(2);

					} else {
						System.out.println("Not Found x");
					}

					String regex2 = "(  <y>)(\\d*\\.\\d*)";
					Pattern r2 = Pattern.compile(regex2);
					Matcher match2 = r2.matcher(s);

					if (match2.find()) {
						y = match2.group(2);

					} else {
						System.out.println("Not Found y");
					}

					String regex3 = "(   <endPointx>)(\\d*\\.\\d*)";
					Pattern r3 = Pattern.compile(regex3);
					Matcher match3 = r3.matcher(s);

					if (match3.find()) {
						endPointx = match3.group(2);

					} else {
						System.out.println("Not Found endPointx");
					}

					String regex4 = "(   <endPointy>)(\\d*\\.\\d*)";
					Pattern r4 = Pattern.compile(regex4);
					Matcher match4 = r4.matcher(s);

					if (match4.find()) {
						endPointy = match4.group(2);

					} else {
						System.out.println("Not Found endPointx");
					}

					String regex5 = "(  <color>)([^\\]]*)";
					Pattern r5 = Pattern.compile(regex5);
					Matcher match5 = r5.matcher(s);

					if (match5.find()) {
						color = match5.group(2);

						String regex6 = "(java\\.awt\\.Color\\[r=)(\\d*)(\\,g=)(\\d*)(\\,b=)(\\d*)";
						Pattern r6 = Pattern.compile(regex6);
						Matcher match6 = r6.matcher(color);
						if (match6.find()) {
							c1 = Integer.parseInt(match6.group(2));
							c2 = Integer.parseInt(match6.group(4));
							c3 = Integer.parseInt(match6.group(6));

						}

					} else {
						System.out.println("Not Found c");
					}

				}
				x1 = Double.parseDouble(x);
				y1 = Double.parseDouble(y);
				endPointx1 = Double.parseDouble(endPointx);
				endPointy1 = Double.parseDouble(endPointy);

				properties.put("startPointx", x1);
				properties.put("startPointy", y1);
				properties.put("endPointx", endPointx1);
				properties.put("endPointy", endPointy1);

				shape.setProperties(properties);
				Color c = new Color(c1, c2, c3);
				shape.setColor(c);

				shapes.add(shape);

				break;
				
			case "Triangle":
				String xt = null;
				String yt = null;
				String secondPointxt = null;
				String secondPointyt = null;
				String endPointxt = null;
				String endPointyt = null;
				String colort = null;
				int c1t = 0;
				int c2t = 0;
				int c3t = 0;

				Double x1t;
				Double y1t;
				Double secondPointx1t;
				Double secondPointy1t;
				Double endPointx1t;
				Double endPointy1t;
				boolean fillt = false;

				shape = new Triangle();
				properties = new HashMap<>();

				for (int i = 0; i < 11; i++) {
					s = br.readLine();

					String regex1 = "(  <x>)(\\d*\\.\\d*)";
					Pattern r1 = Pattern.compile(regex1);
					Matcher match1 = r1.matcher(s);

					if (match1.find()) {
						xt = match1.group(2);

					} else {
						System.out.println("Not Found x");
					}

					String regex2 = "(  <y>)(\\d*\\.\\d*)";
					Pattern r2 = Pattern.compile(regex2);
					Matcher match2 = r2.matcher(s);

					if (match2.find()) {
						yt = match2.group(2);

					} else {
						System.out.println("Not Found y");
					}

					String regex3 = "(   <secondPointx>)(\\d*\\.\\d*)";
					Pattern r3 = Pattern.compile(regex3);
					Matcher match3 = r3.matcher(s);

					if (match3.find()) {
						secondPointxt = match3.group(2);

					} else {
						System.out.println("Not Found endPointx");
					}

					String regex4 = "(   <secondPointy>)(\\d*\\.\\d*)";
					Pattern r4 = Pattern.compile(regex4);
					Matcher match4 = r4.matcher(s);

					if (match4.find()) {
						secondPointyt = match4.group(2);

					} else {
						System.out.println("Not Found endPointx");
					}

					String regex40 = "(   <endPointx>)(\\d*\\.\\d*)";
					Pattern r40 = Pattern.compile(regex40);
					Matcher match40 = r40.matcher(s);

					if (match40.find()) {
						endPointxt = match40.group(2);

					} else {
						System.out.println("Not Found endPointx");
					}

					String regex41 = "(   <endPointy>)(\\d*\\.\\d*)";
					Pattern r41 = Pattern.compile(regex41);
					Matcher match41 = r41.matcher(s);

					if (match41.find()) {
						endPointyt = match41.group(2);

					} else {
						System.out.println("Not Found endPointx");
					}

					String regex5 = "(  <color>)([^ ]*)";
					Pattern r5 = Pattern.compile(regex5);
					Matcher match5 = r5.matcher(s);

					if (match5.find()) {
						colort = match5.group(2);

						String regex6 = "(java\\.awt\\.Color\\[r=)(\\d*)(\\,g=)(\\d*)(\\,b=)(\\d*)";
						Pattern r6 = Pattern.compile(regex6);
						Matcher match6 = r6.matcher(colort);
						if (match6.find()) {
							c1t = Integer.parseInt(match6.group(2));
							c2t = Integer.parseInt(match6.group(4));
							c3t = Integer.parseInt(match6.group(6));

						}

						else {
							System.out.println("Not Found c");
						}

					}
					String regex7 = "(  <fill>)([a-z]*)";
					Pattern r7 = Pattern.compile(regex7);
					Matcher match7 = r7.matcher(s);

					if (match7.find()) {
						fillt = match7.group(2).equals("true");

					} else {
						System.out.println("Not Found fill");
					}

				}
				x1t = Double.parseDouble(xt);
				y1t = Double.parseDouble(yt);
				secondPointx1t = Double.parseDouble(secondPointxt);
				secondPointy1t = Double.parseDouble(secondPointyt);

				endPointx1t = Double.parseDouble(endPointxt);
				endPointy1t = Double.parseDouble(endPointyt);

				properties.put("startPointx", x1t);
				properties.put("startPointy", y1t);
				properties.put("secondPointx", secondPointx1t);
				properties.put("secondPointy", secondPointy1t);
				properties.put("endPointx", endPointx1t);
				properties.put("endPointy", endPointy1t);

				shape.setProperties(properties);
				Color ct = new Color(c1t, c2t, c3t);

				if (fillt) {
					shape.setFillColor(ct);
				} else {
					shape.setColor(ct);
				}

				shapes.add(shape);

				break;
				
			case "Circle":
				String xc = null;
				String yc = null;
				String xAxisc = null;
				String yAxisc = null;
				String colorc = null;
				int c1c = 0;
				int c2c = 0;
				int c3c = 0;

				Double x1c;
				Double y1c;
				Double xAxis1c;
				Double yAxis1c;

				boolean fillc = false;
				shape = new Circle();
				properties = new HashMap<>();

				for (int i = 0; i < 9; i++) {
					s = br.readLine();

					String regex1 = "(  <x>)(\\d*)";
					Pattern r1 = Pattern.compile(regex1);
					Matcher match1 = r1.matcher(s);

					if (match1.find()) {
						xc = match1.group(2);

					} else {
						System.out.println("Not Found x");
					}

					String regex2 = "(  <y>)(\\d*)";
					Pattern r2 = Pattern.compile(regex2);
					Matcher match2 = r2.matcher(s);

					if (match2.find()) {
						yc = match2.group(2);

					} else {
						System.out.println("Not Found y");
					}

					String regex3 = "(   <xAxis>)(\\d*\\.\\d*)";
					Pattern r3 = Pattern.compile(regex3);
					Matcher match3 = r3.matcher(s);

					if (match3.find()) {
						xAxisc = match3.group(2);

					} else {
						System.out.println("Not Found endPointx");
					}

					String regex4 = "(   <yAxis>)(\\d*\\.\\d*)";
					Pattern r4 = Pattern.compile(regex4);
					Matcher match4 = r4.matcher(s);

					if (match4.find()) {
						yAxisc = match4.group(2);

					} else {
						System.out.println("Not Found endPointx");
					}

					String regex5 = "(  <color>)([^ ]*)";
					Pattern r5 = Pattern.compile(regex5);
					Matcher match5 = r5.matcher(s);

					if (match5.find()) {
						colorc = match5.group(2);

						String regex6 = "(java\\.awt\\.Color\\[r=)(\\d*)(\\,g=)(\\d*)(\\,b=)(\\d*)";
						Pattern r6 = Pattern.compile(regex6);
						Matcher match6 = r6.matcher(colorc);
						if (match6.find()) {
							c1c = Integer.parseInt(match6.group(2));
							c2c = Integer.parseInt(match6.group(4));
							c3c = Integer.parseInt(match6.group(6));

						}

					} else {
						System.out.println("Not Found c");
					}

					String regex7 = "(  <fill>)([a-z]*)";
					Pattern r7 = Pattern.compile(regex7);
					Matcher match7 = r7.matcher(s);

					if (match7.find()) {
						fillc = match7.group(2).equals("true");

					} else {
						System.out.println("Not Found fill");
					}

				}
				x1c = Double.parseDouble(xc);
				y1c = Double.parseDouble(yc);
				xAxis1c = Double.parseDouble(xAxisc);
				yAxis1c = Double.parseDouble(yAxisc);

				Point pc = new Point();
				pc.x = (int) (x1c - 0);
				pc.y = (int) (y1c - 0);

				shape.setPosition(pc);

				properties.put("xAxis", xAxis1c);
				properties.put("yAxis", yAxis1c);

				shape.setProperties(properties);
				Color cc = new Color(c1c, c2c, c3c);
				shape.setColor(cc);
				if (fillc) {
					shape.setFillColor(cc);
				}

				shapes.add(shape);

				break;
				
				
				






				
				
				
				
			case "Ellipse":
				String xe = null;
				String ye = null;
				String xAxise = null;
				String yAxise = null;
				String colore = null;
				int c1e = 0;
				int c2e = 0;
				int c3e = 0;

				Double x1e;
				Double y1e;
				Double xAxis1e;
				Double yAxis1e;

				boolean fille = false;
				shape = new Ellipse();
				properties = new HashMap<>();

				for (int i = 0; i < 9; i++) {
					s = br.readLine();

					String regex1 = "(  <x>)(\\d*)";
					Pattern r1 = Pattern.compile(regex1);
					Matcher match1 = r1.matcher(s);

					if (match1.find()) {
						xe = match1.group(2);

					} else {
						System.out.println("Not Found x");
					}

					String regex2 = "(  <y>)(\\d*)";
					Pattern r2 = Pattern.compile(regex2);
					Matcher match2 = r2.matcher(s);

					if (match2.find()) {
						ye = match2.group(2);

					} else {
						System.out.println("Not Found y");
					}

					String regex3 = "(   <xAxis>)(\\d*\\.\\d*)";
					Pattern r3 = Pattern.compile(regex3);
					Matcher match3 = r3.matcher(s);

					if (match3.find()) {
						xAxise = match3.group(2);

					} else {
						System.out.println("Not Found endPointx");
					}

					String regex4 = "(   <yAxis>)(\\d*\\.\\d*)";
					Pattern r4 = Pattern.compile(regex4);
					Matcher match4 = r4.matcher(s);

					if (match4.find()) {
						yAxise = match4.group(2);

					} else {
						System.out.println("Not Found endPointx");
					}

					String regex5 = "(  <color>)([^ ]*)";
					Pattern r5 = Pattern.compile(regex5);
					Matcher match5 = r5.matcher(s);

					if (match5.find()) {
						colore = match5.group(2);

						String regex6 = "(java\\.awt\\.Color\\[r=)(\\d*)(\\,g=)(\\d*)(\\,b=)(\\d*)";
						Pattern r6 = Pattern.compile(regex6);
						Matcher match6 = r6.matcher(colore);
						if (match6.find()) {
							c1e = Integer.parseInt(match6.group(2));
							c2e = Integer.parseInt(match6.group(4));
							c3e = Integer.parseInt(match6.group(6));

						}

					} else {
						System.out.println("Not Found c");
					}

					String regex7 = "(  <fill>)([a-z]*)";
					Pattern r7 = Pattern.compile(regex7);
					Matcher match7 = r7.matcher(s);

					if (match7.find()) {
						fille = match7.group(2).equals("true");

					} else {
						System.out.println("Not Found fill");
					}

				}
				x1e = Double.parseDouble(xe);
				y1e = Double.parseDouble(ye);
				xAxis1e = Double.parseDouble(xAxise);
				yAxis1e = Double.parseDouble(yAxise);

				Point pe = new Point();
				pe.x = (int) (x1e - 0);
				pe.y = (int) (y1e - 0);

				shape.setPosition(pe);

				properties.put("xAxis", xAxis1e);
				properties.put("yAxis", yAxis1e);

				shape.setProperties(properties);
				Color ce = new Color(c1e, c2e, c3e);
				shape.setColor(ce);
				if (fille) {
					shape.setFillColor(ce);
				}

				shapes.add(shape);

				break;
				
			case "Square":
				String xs = null;
				String ys = null;
				String endxs = null;
				String endys = null;
				String colors = null;
				int c1s = 0;
				int c2s = 0;
				int c3s = 0;

				Double x1s;
				Double y1s;
				Double endx1s = null;
				Double endy1s;

				boolean fills = false;
				shape = new Square();
				properties = new HashMap<>();

				for (int i = 0; i < 9; i++) {
					s = br.readLine();

					String regex1 = "(  <x>)(\\d*)";
					Pattern r1 = Pattern.compile(regex1);
					Matcher match1 = r1.matcher(s);

					if (match1.find()) {
						xs = match1.group(2);

					} else {
						System.out.println("Not Found x");
					}

					String regex2 = "(  <y>)(\\d*)";
					Pattern r2 = Pattern.compile(regex2);
					Matcher match2 = r2.matcher(s);

					if (match2.find()) {
						ys = match2.group(2);

					} else {
						System.out.println("Not Found y");
					}

					String regex3 = "(   <endx>)(\\d*\\.\\d*)";
					Pattern r3 = Pattern.compile(regex3);
					Matcher match3 = r3.matcher(s);

					if (match3.find()) {
						endxs = match3.group(2);

					} else {
						System.out.println("Not Found endPointx");
					}

					String regex4 = "(   <endy>)(\\d*\\.\\d*)";
					Pattern r4 = Pattern.compile(regex4);
					Matcher match4 = r4.matcher(s);

					if (match4.find()) {
						endys = match4.group(2);

					} else {
						System.out.println("Not Found endPointx");
					}

					String regex5 = "(  <color>)([^ ]*)";
					Pattern r5 = Pattern.compile(regex5);
					Matcher match5 = r5.matcher(s);

					if (match5.find()) {
						colors = match5.group(2);

						String regex6 = "(java\\.awt\\.Color\\[r=)(\\d*)(\\,g=)(\\d*)(\\,b=)(\\d*)";
						Pattern r6 = Pattern.compile(regex6);
						Matcher match6 = r6.matcher(colors);
						if (match6.find()) {
							c1s = Integer.parseInt(match6.group(2));
							c2s = Integer.parseInt(match6.group(4));
							c3s = Integer.parseInt(match6.group(6));

						}

					} else {
						System.out.println("Not Found c");
					}

					String regex7 = "(  <fill>)([a-z]*)";
					Pattern r7 = Pattern.compile(regex7);
					Matcher match7 = r7.matcher(s);

					if (match7.find()) {
						fills = match7.group(2).equals("true");

					} else {
						System.out.println("Not Found fill");
					}

				}
				
				x1s = Double.parseDouble(xs);
				y1s = Double.parseDouble(ys);
				if(!(endxs == null) && !endxs.equals("null")) {
					endx1s = Double.parseDouble(endxs);
					endy1s = Double.parseDouble(endys);

					properties.put("endx", endx1s);
					properties.put("endy", endy1s);
				}
				else{
					
					
				}

				Point ps = new Point();
				ps.x = (int) (x1s - 0);
				ps.y = (int) (y1s - 0);

				shape.setPosition(ps);


				shape.setProperties(properties);
				Color cs = new Color(c1s, c2s, c3s);
				shape.setColor(cs);
				if (fills) {
					shape.setFillColor(cs);
				}

				shapes.add(shape);

				break;
				
			case "Rectangle":
				String xr = null;
				String yr = null;
				String endxr = null;
				String endyr = null;
				String colorr = null;
				int c1r = 0;
				int c2r = 0;
				int c3r = 0;

				Double x1r;
				Double y1r;
				Double endx1r;
				Double endy1r;

				boolean fillr = false;
				shape = new Rectangle();
				properties = new HashMap<>();

				for (int i = 0; i < 9; i++) {
					s = br.readLine();

					String regex1 = "(  <x>)(\\d*)";
					Pattern r1 = Pattern.compile(regex1);
					Matcher match1 = r1.matcher(s);

					if (match1.find()) {
						xr = match1.group(2);

					} else {
						System.out.println("Not Found x");
					}

					String regex2 = "(  <y>)(\\d*)";
					Pattern r2 = Pattern.compile(regex2);
					Matcher match2 = r2.matcher(s);

					if (match2.find()) {
						yr = match2.group(2);

					} else {
						System.out.println("Not Found y");
					}

					String regex3 = "(   <endx>)(\\d*\\.\\d*)";
					Pattern r3 = Pattern.compile(regex3);
					Matcher match3 = r3.matcher(s);

					if (match3.find()) {
						endxr = match3.group(2);

					} else {
						System.out.println("Not Found endPointx");
					}

					String regex4 = "(   <endy>)(\\d*\\.\\d*)";
					Pattern r4 = Pattern.compile(regex4);
					Matcher match4 = r4.matcher(s);

					if (match4.find()) {
						endyr = match4.group(2);

					} else {
						System.out.println("Not Found endPointx");
					}

					String regex5 = "(  <color>)([^ ]*)";
					Pattern r5 = Pattern.compile(regex5);
					Matcher match5 = r5.matcher(s);

					if (match5.find()) {
						colorr = match5.group(2);

						String regex6 = "(java\\.awt\\.Color\\[r=)(\\d*)(\\,g=)(\\d*)(\\,b=)(\\d*)";
						Pattern r6 = Pattern.compile(regex6);
						Matcher match6 = r6.matcher(colorr);
						if (match6.find()) {
							c1r = Integer.parseInt(match6.group(2));
							c2r = Integer.parseInt(match6.group(4));
							c3r = Integer.parseInt(match6.group(6));

						}

					} else {
						System.out.println("Not Found c");
					}

					String regex7 = "(  <fill>)([a-z]*)";
					Pattern r7 = Pattern.compile(regex7);
					Matcher match7 = r7.matcher(s);

					if (match7.find()) {
						fillr = match7.group(2).equals("true");

					} else {
						System.out.println("Not Found fill");
					}

				}
				x1r = Double.parseDouble(xr);
				y1r = Double.parseDouble(yr);
				endx1r = Double.parseDouble(endxr);
				endy1r = Double.parseDouble(endyr);

				Point p = new Point();
				p.x = (int) (x1r - 0);
				p.y = (int) (y1r - 0);

				shape.setPosition(p);

				if(!(endxr == null)) {
					endx1s = Double.parseDouble(endxr);
					endy1s = Double.parseDouble(endyr);
					properties.put("endx", endx1r);
					properties.put("endy", endy1r);
				}
				
				

				shape.setProperties(properties);
				Color cr = new Color(c1r, c2r, c3r);
				shape.setColor(cr);
				if (fillr) {
					shape.setFillColor(cr);
				}

				shapes.add(shape);

				break;
				
			case "":
				DummyShape d = new DummyShape();
				shapes.add(d);
				
				
				break;
				
			case "RoundRectangle":
				String xrr = null;
				String yrr = null;
				String length = null;
				String arcWidth = null;
				String arcLength = null;
				String width = null;
				String colorrr = null;
				int c1rr = 0;
				int c2rr = 0;
				int c3rr = 0;
				int ff = 0;

				Double x1rr = null;
				Double y1rr = null;
				Double l;
				Double aw;
				Double al;
				Double w;
				
				
				
				

				boolean fillrr = false;
				shape = new RoundRectangle();
				properties = new HashMap<>();

				for (int i = 0; i < 11; i++) {
					s = br.readLine();

					String regex1 = "(  <x>)(-?\\d*)";
					Pattern r1 = Pattern.compile(regex1);
					Matcher match1 = r1.matcher(s);

					if (match1.find()) {
						xrr = match1.group(2);

					} else {
						System.out.println("Not Found x");
					}

					String regex2 = "(  <y>)(-?\\d*)";
					Pattern r2 = Pattern.compile(regex2);
					Matcher match2 = r2.matcher(s);

					if (match2.find()) {
						yrr = match2.group(2);

					} else {
						System.out.println("Not Found y");
					}

					String regex3 = "(   <Length>)(\\d*\\.\\d*)";
					Pattern r3 = Pattern.compile(regex3);
					Matcher match3 = r3.matcher(s);

					if (match3.find()) {
						length = match3.group(2);

					} else {
						System.out.println("Not Found endPointx");
					}

					String regex4 = "(   <ArcWidth>)(\\d*\\.\\d*)";
					Pattern r4 = Pattern.compile(regex4);
					Matcher match4 = r4.matcher(s);

					if (match4.find()) {
						arcWidth = match4.group(2);

					} else {
						System.out.println("Not Found endPointx");
					}

					String regex40 = "(   <ArcLength>)(\\d*\\.\\d*)";
					Pattern r40 = Pattern.compile(regex40);
					Matcher match40 = r40.matcher(s);

					if (match40.find()) {
						arcLength = match40.group(2);

					} else {
						System.out.println("Not Found endPointx");
					}
					
					String regex41 = "(   <ArcLength>)(\\d*\\.\\d*)";
					Pattern r41 = Pattern.compile(regex41);
					Matcher match41 = r41.matcher(s);

					if (match41.find()) {
						arcLength = match41.group(2);

					} else {
						System.out.println("Not Found endPointx");
					}
					
					
					String regex42 = "(   <Width>)(\\d*\\.\\d*)";
					Pattern r42 = Pattern.compile(regex42);
					Matcher match42 = r42.matcher(s);

					if (match42.find()) {
						width = match42.group(2);

					} else {
						System.out.println("Not Found endPointx");
					}
					
					
					
					
					String regex5 = "(  <color>)([^ ]*)";
					Pattern r5 = Pattern.compile(regex5);
					Matcher match5 = r5.matcher(s);

					if (match5.find()) {
						colorrr = match5.group(2);

						String regex6 = "(java\\.awt\\.Color\\[r=)(\\d*)(\\,g=)(\\d*)(\\,b=)(\\d*)";
						Pattern r6 = Pattern.compile(regex6);
						Matcher match6 = r6.matcher(colorrr);
						if (match6.find()) {
							c1rr = Integer.parseInt(match6.group(2));
							c2rr = Integer.parseInt(match6.group(4));
							c3rr = Integer.parseInt(match6.group(6));

						}

					} else {
						c1rr = -1;
						
						
						
					}

					String regex7 = "(  <fillcolor>)([a-z]*)";
					Pattern r7 = Pattern.compile(regex7);
					Matcher match7 = r7.matcher(s);

					if (match7.find()) {
						fillr = match7.group(2).equals("true");

					} else {
						ff = -1;
					}

				}
				
				
				
				Point prr = new Point();
				if(x1rr == null || y1rr == null) {
					prr.x = -1;
					prr.y = -1;
 				}
				else {
					prr.x = (int) (x1rr - 0);
					prr.y = (int) (y1rr - 0);

				}
				
				shape.setPosition(prr);

				l = Double.parseDouble(length);
				al = Double.parseDouble(arcLength);
				aw = Double.parseDouble(arcWidth);
				w = Double.parseDouble(width);
				
				properties.put("Length", l);
				properties.put("ArcLength", al);
				properties.put("ArcWidth", aw);
				properties.put("Width", w);
				

				shape.setProperties(properties);
				Color crr;
				if(c1rr == -1) {
					cr = new Color(-1);
				}
				else {
					cr = new Color(c1rr, c2rr, c3rr);
				}
				
				shape.setColor(cr);
				
				if (ff != -1) {
					shape.setFillColor(cr);
				}

				shapes.add(shape);
				
				
				
				break;

			}

		}

		return shapes;
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
